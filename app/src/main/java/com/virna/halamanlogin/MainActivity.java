package com.virna.halamanlogin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button login;
    EditText ubahNama, ubahPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ubahNama = findViewById(R.id.ubahNama);
        ubahPass = findViewById(R.id.ubahPass);
        login = findViewById(R.id.tombolLogin);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nama = ubahNama.getText().toString();
                String pass = ubahPass.getText().toString();

                if (nama.contains(" ")) {
                    ubahNama.setError("username tidak boleh mengandung spasi");
                } else {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    intent.putExtra("USERNAME", nama);
                    intent.putExtra("PASSWORD", pass);
                    startActivity(intent);
                }
            }
        });
    }
}