package com.virna.halamanlogin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.content.Intent;

public class LoginActivity extends AppCompatActivity {
    TextView resultTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        resultTextView=findViewById(R.id.showResultButton);

        Intent intent = getIntent();
        if (intent!= null){
            String nama = intent.getStringExtra("USERNAME");
            String pass = intent.getStringExtra("PASSWORD");

            if (nama != null && pass != null){
                String resultText = "USERNAME: " + nama + "\nPASSWORD: " + pass;
                resultTextView.setText((resultText));
                resultTextView.setVisibility(View.VISIBLE);
            }
        }
    }
}